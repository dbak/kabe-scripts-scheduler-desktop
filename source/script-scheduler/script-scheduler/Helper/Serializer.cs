﻿using script_scheduler.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace script_scheduler.Helper
{
    public static class Serializer
    {
        static public void SerializeToXML(List<SchedulerTask> tasks)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<SchedulerTask>));
            TextWriter textWriter = new StreamWriter(GetFilePath("tasks.xml"));
            serializer.Serialize(textWriter, tasks);
            textWriter.Close();
        }

        static public List<SchedulerTask> GetSchedulerTasks()
        {
            List<SchedulerTask> tasks = new List<SchedulerTask>();
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<SchedulerTask>));
                TextReader textReader = new StreamReader(GetFilePath("tasks.xml"));
                tasks = (List<SchedulerTask>)deserializer.Deserialize(textReader);
                textReader.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return tasks;
        }



        /// <summary>
        /// Zwraca ściezkę do konkretnego pliku XML z historią wczesniej drukowanych etykiet
        /// jesli plik nie istanieje, to jest tworzony w katalogu uruchomienionych aplikacji
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        static private string GetFilePath(string fileName)
        {
            string dataDir = AppDomain.CurrentDomain.BaseDirectory + @"\Data";
            if (!Directory.Exists(dataDir))
            {
                Directory.CreateDirectory(dataDir);
            }
            string filePath = dataDir + @"\" + fileName;
            if (!File.Exists(filePath))
            {
                var x = File.CreateText(filePath);
                x.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                x.WriteLine("<ArrayOfSchedulerTask xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"></ArrayOfSchedulerTask>");
                x.Flush();
                x.Close();
            }
            return @filePath;
        }
    }
}
