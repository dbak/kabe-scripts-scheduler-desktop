﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace script_scheduler.Model
{
    public class SchedulerTask
    {

        private DateTime? _lastRun;
        private DateTime? _nextRun;

        //private DateTime? _createdAt;
        //private DateTime? _updatedAt;

        public string Name { get; set; }
        public string Path { get; set; }
        public string Arguments { get; set; }
        public DateTime LastRun { get => _lastRun.Value; set => _lastRun = value; }
        public DateTime NextRun { get => _nextRun.Value; set => _nextRun = value; }
        public Frequency Frequency { get; set; }
        //public DateTime CreatedAt { get => _createdAt.Value; set => _createdAt = value; }
        //public DateTime UpdatedAt { get => _updatedAt.Value; set => _updatedAt = value; }

        public string PathWithArguments => string.IsNullOrEmpty(Arguments) ? Path : (Path + " " + Arguments);

        public string FrequencyStr
        {
            get => EnumEX.ToDescriptionString(Frequency);

        }
        public String NextRunStr
        {
            get
            {
                if (!_nextRun.HasValue)
                    return String.Empty;
                if (this.Frequency > Frequency.every_hour)
                    return NextRun.Date.ToString("yyyy-MM-dd");
                else
                    return _nextRun.Value.Date.ToString("yyyy-MM-dd hh:mm:ss");
            }
        }
        public String LastRunStr
        {
            get
            {
                if (!_lastRun.HasValue)
                    return String.Empty;
                return _lastRun.Value.ToString("yyyy-MM-dd hh:mm:ss");
            }
        }

        public void SetLastAndNextRunDates()
        {
           _lastRun = DateTime.Now;
            switch (Frequency)
            {
                case Frequency.every_15_minutes:
                    NextRun = LastRun.AddMinutes(15);
                    break;
                case Frequency.every_30_minutes:
                    NextRun = LastRun.AddMinutes(30);
                    break;
                case Frequency.every_hour:
                    NextRun = LastRun.AddHours(1);
                    break;
                case Frequency.daily:
                    NextRun = LastRun.Date.AddDays(1);
                    break;
                case Frequency.weekly:
                    NextRun = LastRun.Date.AddDays(7);
                    break;
                case Frequency.monthly:
                    NextRun = LastRun.Date.AddMonths(1);
                    break;
            }
        }
    }
}
