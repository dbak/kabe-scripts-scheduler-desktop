﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace script_scheduler.Model
{
    public enum Frequency
    {
        [Description("Every 15 minutes")]
        every_15_minutes = 1,
        [Description("Every 30 minutes")]
        every_30_minutes = 2,
        [Description("Every hour")]
        every_hour = 3,
        [Description("Daily")]
        daily = 4,
        [Description("Weekly")]
        weekly = 5,
        [Description("Monthly")]
        monthly = 6

    }

    public static class EnumEX
    {
        public static string ToDescriptionString(this Frequency val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
