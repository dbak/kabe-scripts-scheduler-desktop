﻿using script_scheduler.Helper;
using script_scheduler.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace script_scheduler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //https://www.codeproject.com/Articles/36468/WPF-NotifyIcon-2
        /// <summary>
        /// https://stackoverflow.com/questions/10230579/easiest-way-to-have-a-program-minimize-itself-to-the-system-tray-using-net-4
        /// </summary>

        private List<SchedulerTask> SchedulerTasks;
        public MainWindow()
        {
            InitializeComponent();
            this.lblVersion.Content = "Script Scheduler ver: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            SchedulerTasks = Serializer.GetSchedulerTasks();
#if DEBUG
            if(!SchedulerTasks.Any())
            {
                SchedulerTasks = new List<SchedulerTask>()
                {
                    new SchedulerTask(){ Name = "123", Path = "xyz", Arguments = "-r -w -q", Frequency = Frequency.every_hour, LastRun = DateTime.Now.AddDays(-1), NextRun = DateTime.Now ,
                    },
                    new SchedulerTask(){ Name = "123_2", Path = "xyz_@", Arguments = "-x- p ", Frequency = Frequency.monthly, LastRun = DateTime.Now.AddMonths(-1), NextRun = DateTime.Now }
                };
            }
#endif 
            dgTasksList.ItemsSource = SchedulerTasks;
            ClearForm();
            RefreshButtonsEnability();
        }

        private void btnFileBrowse_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();

            // Launch OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = openFileDlg.ShowDialog();
            // Get the selected file name and display in a TextBox.
            // Load content of file in a TextBlock
            if (result == true)
            {
                txtPath.Text = openFileDlg.FileName;
            }
        }

        private void ReloadData()
        {
            dgTasksList.Items.Refresh();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SchedulerTasks.Add(new SchedulerTask()
            {
                Name = txtName.Text,
                Path = txtPath.Text,
                Arguments = txtArguments.Text,
                Frequency = (Frequency)cbxFrequency.SelectedIndex+1,
                NextRun = dpTime.SelectedDate.Value
            });
            Serializer.SerializeToXML(SchedulerTasks);
            ClearForm();
            ReloadData();
        }


        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://www.itmore.pl");
            Process.Start(sInfo);
            e.Handled = true;
        }

        private void dgTasksList_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            RefreshButtonsEnability();
            FillForm();
        }

        private void FillForm()
        {
            if (dgTasksList.SelectedItem != null)
            {
                txtName.Text = ((SchedulerTask)dgTasksList.SelectedItem).Name;
                txtArguments.Text = ((SchedulerTask)dgTasksList.SelectedItem).Arguments;
                cbxFrequency.SelectedIndex = (int)(((SchedulerTask)dgTasksList.SelectedItem).Frequency - 1);
                dpTime.SelectedDate = ((SchedulerTask)dgTasksList.SelectedItem).NextRun;
                txtPath.Text = ((SchedulerTask)dgTasksList.SelectedItem).Path;
            }
        }

        private void ClearForm()
        {
            txtName.Text = txtArguments.Text = txtPath.Text = String.Empty;
            cbxFrequency.SelectedIndex = 1;
            dpTime.SelectedDate = DateTime.Now;
        }

        private void RefreshButtonsEnability()
        {
            btnDelete.IsEnabled = btnUpdate.IsEnabled = dgTasksList.SelectedItem != null;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if(dgTasksList.SelectedItem != null)
            {
                SchedulerTasks.Remove((SchedulerTask)dgTasksList.SelectedItem);
                Serializer.SerializeToXML(SchedulerTasks);
                dgTasksList.SelectedItem = null;
                ClearForm();
                ReloadData();
                RefreshButtonsEnability();
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (dgTasksList.SelectedItem != null)
            {
                SchedulerTasks[dgTasksList.SelectedIndex].Name = txtName.Text;
                SchedulerTasks[dgTasksList.SelectedIndex].Path = txtPath.Text;
                SchedulerTasks[dgTasksList.SelectedIndex].Arguments = txtArguments.Text;
                SchedulerTasks[dgTasksList.SelectedIndex].Frequency = (Frequency)cbxFrequency.SelectedIndex + 1;
                SchedulerTasks[dgTasksList.SelectedIndex].NextRun = dpTime.SelectedDate.Value;
                Serializer.SerializeToXML(SchedulerTasks);
                ReloadData();
                RefreshButtonsEnability();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (dgTasksList.SelectedItem != null)
            {

                System.Diagnostics.Process.Start("cmd.exe", @"/c "+ ((SchedulerTask)dgTasksList.SelectedItem).PathWithArguments );
            }
        }
    }
}
